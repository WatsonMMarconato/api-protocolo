var api = {}

api.getAll = function(req, res) {
    var protocolo = [
    	{ _id: getRandom(), value: generateProtocol() }
    ];
    res.json(protocolo);
};

api.error = function(req, res) {
    var error = [
    	{ _id: 1, descricao: 'Você não possui permissão de acesso.' }
    ];
    res.json(error);
};

function generateProtocol() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	s4() + '-' + s4() + s4() + s4();
}

function getRandom(){
	function random(){
		return Math.floor((Math.random() * 9985998) + 1);
	}

	return random();
}
module.exports = api;