module.exports = function(app) {
	var api = app.api.protocolo;
	app.route('/v1/protocolo')
		.get(api.getAll);
	app.route('*')
		.get(api.error);
};