var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var path = require('path');

var app = express();

//cwd: Current Work Directory
consign({cwd: 'application'})
    .include('api')
    .then('routes')
    .into(app);
module.exports = app;

